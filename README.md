# Introduction

Schleimfishe-market is a simulation of a stock market using distributed systems.
This application is made of several java processes that can be launched 
individually but communicate together using sockets, RPC and MOM to simulate
transaction and information exchanges.

# Compile

Since we are using multiple libraries, you need to include the 
classpath of the library in the javac and java commands.  
Here is an example of the command launched at the root of the project :

```
javac -classpath "lib/*" src/*.java
```

```
java -classpath "lib/*" src/example
```

# List of applications

We have created multiple interfaces for the trader, the broker, 
the pricing service and the journalist, below is the list  
of the main files :
- Broker/BrokerGui : Executes the server
- Trader/TraderGui : The basic trader from exercice 1
- Trader/TraderACyclic: The trader who buy when there is bad news
- Trader/TraderCyclic: The trader who buy only when there is good news
- PriceService/PriceServiceGui: Get the current price of a stock or an historic
- NewsService/NewsGui: Launch the publisher and send random news to all traders

# How to use
Executable jar files of all the processes are available in the exec folder. 
To launch all processes at the same time execute FullLaunch.jar.

When a process has started choose the IP address from the drop-down and then click on Connect (Only needed on client type processes).

On basic client you can toogle automatic trading or manually send random request.
On acyclic and cyclic client pushing automatic trading will start the reception of MOM messages.
On new generator it is possible to toogle the automatic creation of random bad and good news
On price service appart from the shiny blenny it is possible to see the current rate of a stock. 
By clicking on "Get historical and lives value" a chart with all historical value updated each second will pop up. 
On the broker it is possible to save historical trades in CSV file.

Error are displayed on the right red console foreach GUI.
If you have any problem, verify you clicked on Connect and you have toogled automatic trading on cyclic and acylic client.


