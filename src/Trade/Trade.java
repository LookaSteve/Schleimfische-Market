/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Trade;

import java.util.Date;

/**
 *
 * @author Karakayn
 */
public class Trade {

    private Date dateOfTransaction;
    private String stockCode;
    private double transactionPrice;
    private String nameOfSeller;
    private String nameOfBuyer;

    public Trade(String stockCode, double price, String seller, String buyer) {
        this.dateOfTransaction = new Date();
        this.stockCode = stockCode;
        this.transactionPrice = price;
        this.nameOfBuyer = buyer;
        this.nameOfSeller = seller;
    }

    /**
     * @return the dateOfTransaction
     */
    public Date getDateOfTransaction() {
        return dateOfTransaction;
    }

    /**
     * @param dateOfTransaction the dateOfTransaction to set
     */
    public void setDateOfTransaction(Date dateOfTransaction) {
        this.dateOfTransaction = dateOfTransaction;
    }

    /**
     * @return the stockCode
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * @param stockCode the stockCode to set
     */
    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * @return the transactionPrice
     */
    public double getTransactionPrice() {
        return transactionPrice;
    }

    /**
     * @param transactionPrice the transactionPrice to set
     */
    public void setTransactionPrice(double transactionPrice) {
        this.transactionPrice = transactionPrice;
    }

    /**
     * @return the nameOfSeller
     */
    public String getNameOfSeller() {
        return nameOfSeller;
    }

    /**
     * @param nameOfSeller the nameOfSeller to set
     */
    public void setNameOfSeller(String nameOfSeller) {
        this.nameOfSeller = nameOfSeller;
    }

    /**
     * @return the nameOfBuyer
     */
    public String getNameOfBuyer() {
        return nameOfBuyer;
    }

    /**
     * @param nameOfBuyer the nameOfBuyer to set
     */
    public void setNameOfBuyer(String nameOfBuyer) {
        this.nameOfBuyer = nameOfBuyer;
    }
}
