/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Trade;

import Core.Client.Client;
import Core.Server.ClientService;
import Trader.Trader;

/**
 * Request comming from Traders
 *
 * @author Karakayn
 */
public class Request {

    private String requestingClientName;
    //Request properties
    private RequestType requestType;
    private double pricePerStock; //price per share
    private int numberWeWantToBuyOrSell; //number of share
    private String stockCode;

    public Request(String reqClient, String type, int number, float price, String stockCode) {
        this.requestingClientName = reqClient;
        ConvertTypeToEnum(type);
        numberWeWantToBuyOrSell = number;
        pricePerStock = price;
        this.stockCode = stockCode;
    }

    private void ConvertTypeToEnum(String type) {
        if (type.equals(RequestType.ASK.toString())) {
            this.setRequestType(RequestType.ASK);
        } else if (type.equals(RequestType.BID.toString())) {
            this.setRequestType(RequestType.BID);
        } else {
            this.setRequestType(RequestType.UNKNOWN);
        }
    }

    /**
     * @return the requestType
     */
    public RequestType getRequestType() {
        return requestType;
    }

    /**
     * @param requestType the requestType to set
     */
    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    /**
     * @return the pricePerStock
     */
    public double getPricePerStock() {
        return pricePerStock;
    }

    /**
     * @param pricePerStock the pricePerStock to set
     */
    public void setPricePerStock(double pricePerStock) {
        this.pricePerStock = pricePerStock;
    }

    /**
     * @return the numberWeWantToBuyOrSell
     */
    public int getNumberWeWantToBuyOrSell() {
        return numberWeWantToBuyOrSell;
    }

    /**
     * @param numberWeWantToBuyOrSell the numberWeWantToBuyOrSell to set
     */
    public void setNumberWeWantToBuyOrSell(int numberWeWantToBuyOrSell) {
        this.numberWeWantToBuyOrSell = numberWeWantToBuyOrSell;
    }

    /**
     * @return the stockCode
     */
    public String getStockCode() {
        return stockCode;
    }

    /**
     * @param stockCode the stockCode to set
     */
    public void setStockCode(String stockCode) {
        this.stockCode = stockCode;
    }

    /**
     * @return the requestingClientName
     */
    public String getRequestingClientName() {
        return requestingClientName;
    }

    /**
     * @param requestingClientName the requestingClientName to set
     */
    public void setRequestingClientName(String requestingClientName) {
        this.requestingClientName = requestingClientName;
    }

}
