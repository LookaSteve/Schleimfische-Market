/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Trade;

import java.util.ArrayList;

/**
 *
 * @author Karakayn
 */
public class Stock {

    private String code;
    private String name;
    private BrokerRequest hightestBid;
    private BrokerRequest lowestAsk;
    private double lastTransictionPrice;
    private ArrayList<BrokerRequest> bidRequest;
    private ArrayList<BrokerRequest> askRequest;

    public Stock(String code, String name) {

        this.code = code;
        this.name = name;
        hightestBid = null;
        lowestAsk = null;
        lastTransictionPrice = 0;
        bidRequest = new ArrayList<BrokerRequest>();
        askRequest = new ArrayList<BrokerRequest>();

    }

    /**
     * Check if the bid request contain a higher bid or not
     *
     * @Return true if we find one
     */
    public boolean CheckBid(BrokerRequest request) {
        if (this.getHightestBid() == null) {
            this.setHightestBid(request);
            return false;
        }

        if (request.getRequest().getRequestType().equals(RequestType.BID)) {
            if (request.getRequest().getPricePerStock() > this.getHightestBid().getRequest().getPricePerStock()) {
                this.setHightestBid(request);
                return true;
            } else {
                return false;
            }
        } else {
            throw new IllegalStateException("We check for a bid request but it's not a bid request...");
        }
    }

    /**
     * Check if the ask request contain a lowest ask or not
     *
     * @Return true if we find one
     */
    public boolean CheckAsk(BrokerRequest request) {
        if (this.getLowestAsk() == null) {
            this.setLowestAsk(request);
            return false;
        }

        if (request.getRequest().getRequestType().equals(RequestType.ASK)) {
            if (request.getRequest().getPricePerStock() > this.getLowestAsk().getRequest().getPricePerStock()) {
                this.setLowestAsk(request);
                return true;
            } else {
                return false;
            }

        } else {
            throw new IllegalStateException("We check for a ask request but it's not a ask request...");
        }
    }

    /**
     * Check in list of Bidrequest the hightest
     *
     * @return Hightest bid request
     */
    public BrokerRequest FindHightestBidRequestInList() {
        for (int i = 0; i < this.bidRequest.size(); i++) {
            BrokerRequest currentBidRequest = this.bidRequest.get(i);
            if (currentBidRequest.getRequest().getPricePerStock() == this.hightestBid.getRequest().getPricePerStock()) {
                return currentBidRequest;
            }
        }

        throw new IllegalStateException("Cannot find hightest bid request");
    }

    /**
     * Check in list of AskRequest the lowest ask request
     *
     * @return the lowest ask request
     */
    public BrokerRequest FindLowestAskRequestInList() {
        for (int i = 0; i < this.askRequest.size(); i++) {
            BrokerRequest currentAskRequest = this.askRequest.get(i);
            if (currentAskRequest.getRequest().getPricePerStock() == this.lowestAsk.getRequest().getPricePerStock()) {
                return currentAskRequest;
            }
        }

        throw new IllegalStateException("Cannot find lowest ask request");
    }

    /**
     * This function allow us to refill the lowest ask and the bigest bid when
     * we just deleted two of those request
     */
    public void RefillLowestAskAndBigestBid() {
        //lowest ask
        if (this.askRequest == null) {
            this.lowestAsk = null;
        } else {
            if (this.askRequest.isEmpty()) {
                this.lowestAsk = null;
            } else {
                BrokerRequest tempLowest = this.askRequest.get(0);
                for (int i = 1; i < this.askRequest.size(); i++) {
                    BrokerRequest currentAskRequest = this.askRequest.get(i);
                    if (tempLowest.getRequest().getPricePerStock() > currentAskRequest.getRequest().getPricePerStock()) {
                        tempLowest = currentAskRequest; //we find lower so we save it
                    }
                }
                //finally we have the lowest so we add it to the good property
                this.lowestAsk = tempLowest;
            }
        }

        //biggest bid
        if (this.bidRequest == null) {
            this.hightestBid = null;
        } else {
            if (this.bidRequest.isEmpty()) {
                this.hightestBid = null;
            } else {
                BrokerRequest tempHighest = this.bidRequest.get(0);
                for (int i = 1; i < this.bidRequest.size(); i++) {
                    BrokerRequest currentBidRequest = this.bidRequest.get(i);
                    if (tempHighest.getRequest().getPricePerStock() < currentBidRequest.getRequest().getPricePerStock()) {
                        tempHighest = currentBidRequest; //we find biggest so we save it
                    }
                }
                //finally we have the lowest so we add it to the good property
                this.hightestBid = tempHighest;
            }
        }
    }

    /**
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * @param code the code to set
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the hightestBid
     */
    public BrokerRequest getHightestBid() {
        return hightestBid;
    }

    /**
     * @param hightestBid the hightestBid to set
     */
    public void setHightestBid(BrokerRequest hightestBid) {
        this.hightestBid = hightestBid;
    }

    /**
     * @return the lowestAsk
     */
    public BrokerRequest getLowestAsk() {
        return lowestAsk;
    }

    /**
     * @param lowestAsk the lowestAsk to set
     */
    public void setLowestAsk(BrokerRequest lowestAsk) {
        this.lowestAsk = lowestAsk;
    }

    /**
     * @return the bidRequest
     */
    public ArrayList<BrokerRequest> getBidRequest() {
        return bidRequest;
    }

    /**
     * @param bidRequest the bidRequest to set
     */
    public void setBidRequest(ArrayList<BrokerRequest> bidRequest) {
        this.bidRequest = bidRequest;
    }

    /**
     * @return the askRequest
     */
    public ArrayList<BrokerRequest> getAskRequest() {
        return askRequest;
    }

    public void setAskRequest(ArrayList<BrokerRequest> askRequest) {
        this.askRequest = askRequest;
    }

    /**
     * @return the lastTransactionPrice
     */
    public double getLastTransictionPrice() {
        return lastTransictionPrice;
    }

    /**
     * @param lastTransactionPrice the lastTransactionPrice to set
     */
    public void setLastTransictionPrice(double lastTransictionPrice) {
        this.lastTransictionPrice = lastTransictionPrice;
    }
}
