package Trade;

import Core.Server.ClientService;

public class BrokerRequest {

    private ClientService client;
    private Request request;

    public BrokerRequest(ClientService client, Request request) {
        this.client = client;
        this.request = request;
    }

    public ClientService getClient() {
        return client;
    }

    public void setClient(ClientService client) {
        this.client = client;
    }

    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }

}
