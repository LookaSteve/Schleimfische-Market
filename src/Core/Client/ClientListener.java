package Core.Client;

public interface ClientListener {

    public void clientInfoMessage(String message);

    public void clientErrorMessage(String message);

    public void clientServerMessage(Client server, String message);

}
