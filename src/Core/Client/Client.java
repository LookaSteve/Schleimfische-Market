package Core.Client;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class Client extends Thread {

    private Socket socket;

    private String address;
    private int port;
    private ClientListener listener;

    private boolean isActive = true;
    private DataOutputStream toServer;
    private BufferedReader fromServer;

    public Client(String address, int port, ClientListener listener) {

        this.address = address;
        this.port = port;
        this.listener = listener;

        this.start();

    }

    @Override
    public void run() {

        try {

            socket = new Socket(address, port);

            toServer = new DataOutputStream(socket.getOutputStream());
            fromServer = new BufferedReader(new InputStreamReader(socket.getInputStream()));

            infoMessage("Client started at " + address + "@" + port);

            while (isActive) {

                String message = new String(fromServer.readLine()) + '\n';

                if (listener != null) {
                    listener.clientServerMessage(this, message);
                }

            }

            infoMessage("Client requested end.");

        } catch (IOException e) {
            e.printStackTrace();
            errorMessage(e.getMessage());
            errorMessage("Client termination.");
        }

        //Safely close everything
        endClient();

        infoMessage("Client terminated.");

    }

    private void endClient() {
        try {
            socket.close();
        } catch (IOException e) {
            e.printStackTrace();
            errorMessage(e.getMessage());
        }
        try {
            toServer.close();
        } catch (IOException e) {
            e.printStackTrace();
            errorMessage(e.getMessage());
        }
        try {
            fromServer.close();
        } catch (IOException e) {
            e.printStackTrace();
            errorMessage(e.getMessage());
        }
    }

    public synchronized void sendData(String data) {
        if (toServer != null) {
            try {
                toServer.writeBytes(data + '\n');
            } catch (IOException e) {
                e.printStackTrace();
                endClient();
            }
        }
    }

    public void stopClean() {
        isActive = false;
    }

    private void infoMessage(String message) {
        if (listener != null) {
            listener.clientInfoMessage(message);
        }
    }

    private void errorMessage(String message) {
        if (listener != null) {
            listener.clientErrorMessage(message);
        }
    }

}
