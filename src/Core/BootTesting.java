package Core;

import Broker.Broker;
import Broker.BrokerGui;
import Trader.TraderGui;
import java.awt.Dimension;
import java.awt.Toolkit;

public class BootTesting {

    public static void main(String[] args) {

        BrokerGui brokerGUI = new BrokerGui();
        //place brokerGUI at top right
        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        brokerGUI.setLocation(dim.width - brokerGUI.getWidth(), 0);
        brokerGUI.setVisible(true);
        Broker broker = new Broker(brokerGUI);

        TraderGui simpleTraderGUI = new TraderGui();
        simpleTraderGUI.setLocation(dim.width - simpleTraderGUI.getWidth() * 2, 0);
        simpleTraderGUI.setVisible(true);

    }

}
