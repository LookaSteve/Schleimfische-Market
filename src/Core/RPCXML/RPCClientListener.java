package Core.RPCXML;

public interface RPCClientListener {

    public void RPCClientMessage(String message);

    public void RPCClientError(String message);

}
