package Core.RPCXML;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import org.apache.xmlrpc.XmlRpcException;
import org.apache.xmlrpc.client.XmlRpcClient;
import org.apache.xmlrpc.client.XmlRpcClientConfigImpl;

import PriceService.PriceServiceGui;
import Tools.Addresses;

public class RPCXMLClient {

    private XmlRpcClient client;
    private RPCClientListener rpcClientListener;

    public RPCXMLClient() {
        //KEEP EMPTY !
    }

    public RPCXMLClient(RPCClientListener rpcClientListener, String address) {

        this.rpcClientListener = rpcClientListener;

        try {

            XmlRpcClientConfigImpl config = new XmlRpcClientConfigImpl();
            config.setServerURL(new URL("http://" + address + ":" + Addresses.PORT_RPCXML + "/xmlrpc"));

            client = new XmlRpcClient();
            client.setConfig(config);

        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

    }

    public double singleStockPrice(String code) {

        double result = 0;

        if (client != null) {

            try {

                Object[] params = new Object[]{new String(code)};
                rpcClientListener.RPCClientMessage("About to get stock price for : = " + params[0]);

                result = (Double) client.execute("RPC.singleStockPrice", params);
                rpcClientListener.RPCClientMessage("Price = " + result);
            } catch (XmlRpcException e) {
                rpcClientListener.RPCClientError(e.getMessage());
                e.printStackTrace();
            }

        }

        return result;

    }

    public String[] getStockNames() {

        String[] result = {};

        if (client != null) {

            try {

                Object[] params = new Object[0];

                rpcClientListener.RPCClientMessage("About to get stock names");

                String raw = (String) client.execute("RPC.getStockNames", params);

                result = raw.split("#");

                rpcClientListener.RPCClientMessage("Stocks = " + raw);

            } catch (XmlRpcException e) {
                rpcClientListener.RPCClientError(e.getMessage());
                e.printStackTrace();
            }

        }

        return result;

    }

    public String getStockName(String code) {

        String result = "";

        if (client != null) {

            try {

                Object[] params = new Object[]{new String(code)};
                rpcClientListener.RPCClientMessage("About to get name for : = " + params[0]);

                result = (String) client.execute("RPC.getStockName", params);
                rpcClientListener.RPCClientMessage("Name = " + result);
            } catch (XmlRpcException e) {
                rpcClientListener.RPCClientError(e.getMessage());
                e.printStackTrace();
            }

        }

        return result;

    }

    public String[] getStockHistory(String code) {

        String[] result = {};

        if (client != null) {

            try {

                Object[] params = new Object[]{new String(code)};

                rpcClientListener.RPCClientMessage("About to get stock history");

                String raw = (String) client.execute("RPC.getStockHistory", params);

                result = raw.split("#");

                rpcClientListener.RPCClientMessage("History = " + raw);

            } catch (XmlRpcException e) {
                rpcClientListener.RPCClientError(e.getMessage());
                e.printStackTrace();
            }

        }

        return result;

    }

    public String getLastTransaction(String code) {

        String result = "";

        if (client != null) {

            try {

                Object[] params = new Object[]{new String(code)};

                rpcClientListener.RPCClientMessage("About to get stock last transaction");

                result = (String) client.execute("RPC.getLastTransaction", params);

                rpcClientListener.RPCClientMessage("Last transaction = " + result);

            } catch (XmlRpcException e) {
                rpcClientListener.RPCClientError(e.getMessage());
                e.printStackTrace();
            }

        }

        return result;

    }

}
