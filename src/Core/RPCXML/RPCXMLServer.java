package Core.RPCXML;

import java.util.ArrayList;

import org.apache.log4j.BasicConfigurator;
import org.apache.xmlrpc.server.PropertyHandlerMapping;
import org.apache.xmlrpc.server.XmlRpcServer;
import org.apache.xmlrpc.server.XmlRpcServerConfigImpl;
import org.apache.xmlrpc.webserver.WebServer;

import Broker.BrokerGui;
import Tools.Addresses;
import Trade.Stock;

public class RPCXMLServer {

    public RPCXMLServer() {
        //KEEP EMPTY !
    }

    public void start() {

        try {

            BasicConfigurator.configure();

            WebServer webServer = new WebServer(Addresses.PORT_RPCXML);
            XmlRpcServer xmlRpcServer = webServer.getXmlRpcServer();
            PropertyHandlerMapping phm = new PropertyHandlerMapping();
            phm.addHandler("RPC", RPCXMLServer.class);
            xmlRpcServer.setHandlerMapping(phm);
            XmlRpcServerConfigImpl serverConfig = (XmlRpcServerConfigImpl) xmlRpcServer.getConfig();
            serverConfig.setEnabledForExtensions(true);
            serverConfig.setContentLengthOptional(false);

            webServer.start();

            BrokerGui.broker.printMessage("RPCXML Service is live.");

        } catch (Exception e) {

            BrokerGui.broker.printError("RPCXML Service exception: " + e);

        }

    }

    public Double singleStockPrice(String code) {

        double price = 0;

        if (BrokerGui.broker != null) {

            BrokerGui.broker.printMessage("RPCXML price request [" + code + "]");

            Stock foundStock = BrokerGui.broker.findStockByCode(code);

            if (foundStock != null) {

                price = foundStock.getLastTransictionPrice();

                BrokerGui.broker.printMessage("Found price: " + price + ".");

            } else {

                BrokerGui.broker.printMessage("Code not found.");

            }

        }

        return price;
    }

    public String getStockNames() {

        String result = "";

        if (BrokerGui.broker != null) {

            BrokerGui.broker.printMessage("RPCXML request stock names");

            ArrayList<Stock> stockList = BrokerGui.broker.getStocks();

            for (int i = 0; i < stockList.size(); i++) {

                if (i != stockList.size() - 1) {
                    result += stockList.get(i).getCode() + "#";
                } else {
                    result += stockList.get(i).getCode();
                }

            }

            BrokerGui.broker.printMessage("Found names: " + result + ".");

        }

        return result;
    }

    public String getStockName(String code) {

        String name = "";

        if (BrokerGui.broker != null) {

            BrokerGui.broker.printMessage("RPCXML name request [" + code + "]");

            Stock foundStock = BrokerGui.broker.findStockByCode(code);

            if (foundStock != null) {

                name = foundStock.getName();

                BrokerGui.broker.printMessage("Found name: " + name + ".");

            } else {

                BrokerGui.broker.printMessage("Code not found.");

            }

        }

        return name;
    }

    public String getStockHistory(String code) {

        String result = "";

        if (BrokerGui.broker != null) {

            BrokerGui.broker.printMessage("RPCXML request stock history");

            ArrayList<String> historic = BrokerGui.broker.getHistoricValuesByCode(code);

            for (int i = 0; i < historic.size(); i++) {

                if (i != historic.size() - 1) {
                    result += historic.get(i) + "#";
                } else {
                    result += historic.get(i);
                }

            }

            BrokerGui.broker.printMessage("Found history: " + result + ".");

        }

        return result;
    }

    public String getLastTransaction(String code) {

        String result = "";

        if (BrokerGui.broker != null) {

            BrokerGui.broker.printMessage("RPCXML request stock last transaction");

            ArrayList<String> historic = BrokerGui.broker.getHistoricValuesByCode(code);

            if (historic.size() > 0) {

                result = historic.get(historic.size() - 1);

            }

            BrokerGui.broker.printMessage("Found last transaction: " + result + ".");

        }

        return result;
    }

}
