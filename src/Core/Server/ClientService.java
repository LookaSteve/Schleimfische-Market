package Core.Server;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientService extends Thread {

    private Server parentServer;

    private Socket clientSocket;
    private DataOutputStream toClient;
    private BufferedReader fromClient;

    private boolean isActive = true;

    public ClientService(Server parentServer, Socket clientSocket) {

        this.clientSocket = clientSocket;
        this.parentServer = parentServer;

    }

    @Override
    public void run() {

        try {

            System.out.println("Thread started: " + this);

            fromClient = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

            toClient = new DataOutputStream(clientSocket.getOutputStream());

            while (isActive) {

                String line = fromClient.readLine();
                System.out.println("Received: " + line);
                parentServer.clientMessage(this, line);

            }

            System.out.println("Thread ended: " + this);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //Safely close everything
        endClient();

    }

    private void endClient() {
        try {
            fromClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            toClient.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            clientSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void sendData(String data) {
        if (toClient != null) {
            try {
                toClient.writeBytes(data + '\n');
            } catch (IOException e) {
                e.printStackTrace();
                endClient();
            }
        }
    }

}
