package Core.Server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server extends Thread {

    private boolean isActive = true;
    private int port;
    private ServerListener listener;

    public Server(int port, ServerListener listener) {
        this.port = port;
        this.listener = listener;
    }

    @Override
    public void run() {

        try {
            ServerSocket listenSocket = new ServerSocket(port);
            infoMessage("Server started on port " + port);

            while (isActive) {

                Socket client;
                client = listenSocket.accept();
                infoMessage("New client : " + client.getRemoteSocketAddress() + "@" + port);
                new ClientService(this, client).start();

            }

            infoMessage("Server stopped on port " + port);

        } catch (IOException e) {

            e.printStackTrace();
            errorMessage(e.getMessage());
        }

    }

    public void stopClean() {
        isActive = false;
    }

    private void infoMessage(String message) {
        if (listener != null) {
            listener.serverInfoMessage(message);
        }
    }

    private void errorMessage(String message) {
        if (listener != null) {
            listener.serverErrorMessage(message);
        }
    }

    public void clientMessage(ClientService client, String message) {
        if (listener != null) {
            listener.serverClientMessage(client, message);
        }
    }

}
