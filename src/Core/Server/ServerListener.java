package Core.Server;

public interface ServerListener {

    public void serverInfoMessage(String message);

    public void serverErrorMessage(String message);

    public void serverClientMessage(ClientService client, String message);

}
