/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import Broker.Broker;
import Broker.BrokerGui;
import NewsService.NewsGui;
import PriceService.PriceServiceGui;
import Trader.TraderACyclicGui;
import Trader.TraderCyclicGui;
import Trader.TraderGui;
import java.awt.Dimension;
import java.awt.Toolkit;

/**
 *
 * @author Karakayn
 */
public class FullLaunch {

    public static void main(String[] args) {

        //Broker
        BrokerGui brokerGUI = new BrokerGui();

        Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
        brokerGUI.setLocation(dim.width - brokerGUI.getWidth(), 0);
        brokerGUI.setVisible(true);

        Broker broker = new Broker(brokerGUI);
        brokerGUI.setBroker(broker);

        broker.startXMLRPC();
        //place brokerGUI at top right

        //Trader simple
        TraderGui simpleTraderGUI = new TraderGui();
        simpleTraderGUI.setLocation(dim.width - simpleTraderGUI.getWidth() * 2, 0);
        simpleTraderGUI.setVisible(true);

        //Trader cyclic
        TraderCyclicGui traderCyclicGUI = new TraderCyclicGui();
        traderCyclicGUI.setLocation(dim.width - traderCyclicGUI.getWidth(), traderCyclicGUI.getHeight());
        traderCyclicGUI.setVisible(true);

        //Trader acyclic
        TraderACyclicGui traderACyclicGUI = new TraderACyclicGui();
        traderACyclicGUI.setLocation(dim.width - traderACyclicGUI.getWidth() * 2, traderACyclicGUI.getHeight());
        traderACyclicGUI.setVisible(true);

        //Publisher (make news!)
        NewsGui newsGUI = new NewsGui();
        newsGUI.setLocation(dim.width - newsGUI.getWidth() * 3, newsGUI.getHeight());
        newsGUI.setVisible(true);

        //Price service 
        PriceServiceGui priceService = new PriceServiceGui();
        priceService.setLocation(0, 0);
        priceService.setVisible(true);
    }

}
