package Core;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import Tools.Console;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.UUID;
import java.awt.event.ActionEvent;

public class Demo extends JFrame {

    private JPanel contentPane;
    private Console console;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    Demo frame = new Demo();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public Demo() {
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        console = new Console();
        contentPane.add(console, BorderLayout.CENTER);

        JButton btnAddRandom = new JButton("Add random");
        btnAddRandom.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                console.log(generateString());
                console.logErr(generateString());

            }
        });

        contentPane.add(btnAddRandom, BorderLayout.NORTH);

    }

    public static String generateString() {
        return UUID.randomUUID().toString();
    }

}
