package Core.MOM;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;

import javax.jms.*;
import Tools.Addresses;

/**
 * Created by Momo on 01/03/2018.
 */
public class MOMServer {

    private Connection connection;
    private Session session;
    private MessageProducer producer;

    public MOMServer() throws JMSException {
        String user = env("ACTIVEMQ_USER", "admin");
        String password = env("ACTIVEMQ_PASSWORD", "password");
        String host = env("ACTIVEMQ_HOST", "localhost");
        int port = Integer.parseInt(env("ACTIVEMQ_PORT", Integer.toString(Addresses.PORT_MOM)));
        String destination = "event";

        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);
        this.connection = factory.createConnection(user, password);
        this.connection.start();
        this.session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);

        Destination dest = new ActiveMQTopic(destination);
        this.producer = session.createProducer(dest);
        this.producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
    }

    public void sendNews(String code, String body, int type) throws JMSException {
        TextMessage msg = this.session.createTextMessage(body);
        msg.setIntProperty("typeOfNews", type);
        msg.setStringProperty("codeStock", code);
        this.producer.send(msg);
    }

    private static String env(String key, String defaultValue) {
        String rc = System.getenv(key);
        if (rc == null) {
            return defaultValue;
        }
        return rc;
    }

    private static String arg(String[] args, int index, String defaultValue) {
        if (index < args.length) {
            return args[index];
        } else {
            return defaultValue;
        }
    }
}
