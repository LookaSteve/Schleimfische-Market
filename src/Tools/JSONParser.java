package Tools;

import com.google.gson.*;
import Trade.Request;

/**
 * Created by Momo on 26/02/2018.
 */
public class JSONParser {

    public String fromObjectToString(Request obj) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        String jsonInString = gson.toJson(obj);
        return jsonInString;
    }

    public Request fromStringToObject(String data) {
        GsonBuilder builder = new GsonBuilder();
        Gson gson = builder.create();
        Request obj = gson.fromJson(data, Request.class);
        return obj;
    }
}
