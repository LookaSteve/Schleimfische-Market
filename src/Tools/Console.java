package Tools;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.text.DefaultCaret;

import java.awt.BorderLayout;
import java.awt.Color;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JSplitPane;
import java.awt.Font;

public class Console extends JPanel {

    private JTextArea logArea;
    private JTextArea errArea;

    public Console() {
        setLayout(new BorderLayout(0, 0));

        JButton btnNewButton = new JButton("Clear");
        btnNewButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                clear();

            }
        });
        add(btnNewButton, BorderLayout.SOUTH);

        JSplitPane splitPane = new JSplitPane();
        splitPane.setResizeWeight(0.5);
        splitPane.setEnabled(false);
        add(splitPane, BorderLayout.CENTER);

        JScrollPane scrollPane = new JScrollPane();
        splitPane.setLeftComponent(scrollPane);

        logArea = new JTextArea();
        logArea.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 14));
        logArea.setEnabled(false);
        logArea.setBackground(Color.BLACK);
        logArea.setForeground(Color.GREEN);
        logArea.setDisabledTextColor(Color.GREEN);
        scrollPane.setViewportView(logArea);

        JScrollPane scrollPane_1 = new JScrollPane();
        splitPane.setRightComponent(scrollPane_1);

        errArea = new JTextArea();
        errArea.setFont(new Font("Arial Rounded MT Bold", Font.PLAIN, 14));
        errArea.setEnabled(false);
        errArea.setBackground(Color.BLACK);
        errArea.setForeground(Color.RED);
        errArea.setDisabledTextColor(Color.RED);
        scrollPane_1.setViewportView(errArea);

        DefaultCaret caretlog = (DefaultCaret) logArea.getCaret();
        caretlog.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

        DefaultCaret careterr = (DefaultCaret) errArea.getCaret();
        careterr.setUpdatePolicy(DefaultCaret.ALWAYS_UPDATE);

    }

    public void log(String str) {
        logRaw(str + System.lineSeparator());
    }

    public void logErr(String str) {
        logErrRaw(str + System.lineSeparator());
    }

    public void logRaw(String str) {
        logArea.append(str);
    }

    public void logErrRaw(String str) {
        errArea.append(str);
    }

    public void clear() {
        logArea.setText("");
        errArea.setText("");
    }

}
