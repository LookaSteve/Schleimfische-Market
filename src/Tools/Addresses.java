package Tools;

public class Addresses {

    public final static int PORT = 42417;
    public final static int PORT_MOM = 61616;
    public final static int PORT_RPCXML = 8080;

    public final static String IP_PHIL = "172.17.1.73";
    public final static String IP_JOJO = "172.17.1.187";
    public final static String IP_MOMO = "172.17.1.239";
    public final static String IP_ANTO = "172.17.2.4";

}
