package Broker;

import Core.RPCXML.RPCXMLServer;
import Core.Server.ClientService;
import Core.Server.Server;
import Core.Server.ServerListener;
import Tools.Addresses;
import Tools.JSONParser;
import Trade.BrokerRequest;
import Trade.Request;
import Trade.RequestType;
import Trade.Stock;
import Trade.Trade;
import Trader.Trader;
import java.util.ArrayList;

/**
 * The broker should keep track of the highest bid and the lowest ask seen for
 * each stock. A trade takes place only when the highest bid is greater than or
 * equal to the lowest ask.
 *
 * @author Karakayn
 */
public class Broker implements ServerListener {

    private BrokerGui gui;
    private RPCXMLServer rpcxmlServer;
    private static ArrayList<Stock> stocks;
    private ArrayList<Trade> tradeProcessed;

    public Broker(BrokerGui gui) {

        this.gui = gui;
        this.tradeProcessed = new ArrayList<Trade>();

        stocks = new ArrayList();
        stocks.add(new Stock("AAPL", "Apple"));
        stocks.add(new Stock("IBM", "IBM"));
        stocks.add(new Stock("MSFT", "Microsoft"));
        stocks.add(new Stock("ORCL", "Oracle Corporations"));
        stocks.add(new Stock("TSLA", "Tesla inc"));

        startServer();
    }

    private void startServer() {

        Server server = new Server(Addresses.PORT, this);
        server.start();

    }

    public void startXMLRPC() {

        rpcxmlServer = new RPCXMLServer();
        rpcxmlServer.start();

    }

    public void HandleRequest(BrokerRequest brokerRequest) {
        if (brokerRequest.getRequest().getRequestType().equals(RequestType.ASK)) {
            HandleAskRequest(brokerRequest);
        } else if (brokerRequest.getRequest().getRequestType().equals(RequestType.BID)) {
            HandleBidRequest(brokerRequest);
        } else {
            throw new IllegalStateException("HandleRequest > Unknown type of request");
        }
    }

    public void SendErrorMessageToTrader(Trader trader) {
        System.out.println("Error message ...");
    }

    private void HandleAskRequest(BrokerRequest brokerRequest) {
        if (brokerRequest.getRequest().getRequestType().equals(RequestType.ASK)) {
            Stock askedStock = FindStock(brokerRequest.getRequest().getStockCode());
            if (askedStock != null) {

                //add our request to the list of the good stock
                askedStock.getAskRequest().add(brokerRequest);
                //check to fill lowerask if we are lower
                if (askedStock.CheckAsk(brokerRequest)) {

                    if (askedStock.getLowestAsk() != null && askedStock.getHightestBid() != null) {
                        //check if we can begin a trade
                        if (askedStock.getHightestBid().getRequest().getPricePerStock() >= askedStock.getLowestAsk().getRequest().getPricePerStock()) {
                            StartTrade(askedStock, brokerRequest);
                        } //we are not eligible to start a trade... not matching
                    }

                }

            } else {
                throw new NullPointerException("HandleAskRequest > askedStock is null");

            }
        } else {
            throw new IllegalStateException("HandleAskRequest > It's not an ask request");
        }
    }

    private void HandleBidRequest(BrokerRequest brokerRequest) {
        if (brokerRequest.getRequest().getRequestType().equals(RequestType.BID)) {
            Stock askedStock = FindStock(brokerRequest.getRequest().getStockCode());
            if (askedStock != null) {

                //add our request to the list of the good stock
                askedStock.getBidRequest().add(brokerRequest);
                //check to fill lowerask if we are lower
                //check if we can begin a trade

                if (askedStock.CheckBid(brokerRequest)) {
                    if (askedStock.getLowestAsk() != null && askedStock.getHightestBid() != null) {
                        if (askedStock.getHightestBid().getRequest().getPricePerStock() >= askedStock.getLowestAsk().getRequest().getPricePerStock()) {
                            StartTrade(askedStock, brokerRequest);
                        } //we are not eligible to start a trade... not matching
                    }

                }

            } else {
                throw new NullPointerException("HandleBidRequest > askedStock is null");
            }
        } else {
            throw new IllegalStateException("HandleBidRequest > It's not a bid request");
        }
    }

    private Stock FindStock(String stockCode) {
        if (getStocks() != null) {
            for (int i = 0; i < getStocks().size(); i++) {
                Stock currentStock = getStocks().get(i);
                if (currentStock.getCode().equals(stockCode)) {
                    return currentStock;
                }
            }
            //we didn't find any stock with this code so we create it
            Stock newStock = new Stock(stockCode, stockCode);
            return newStock;
        } else {
            throw new NullPointerException("FindStock > Stock was null");
        }
    }

    public Stock findStockByCode(String stockCode) {
        for (int i = 0; i < stocks.size(); i++) {
            if (stocks.get(i).getCode().equals(stockCode)) {
                return stocks.get(i);
            }
        }
        return null;
    }

    public ArrayList<String> getHistoricValuesByCode(String code) {

        ArrayList<String> values = new ArrayList<>();

        for (int i = 0; i < tradeProcessed.size(); i++) {

            if (tradeProcessed.get(i).getStockCode().equals(code)) {

                values.add(tradeProcessed.get(i).getDateOfTransaction().getTime() + "|" + tradeProcessed.get(i).getTransactionPrice());

            }

        }

        return values;

    }

    private void StartTrade(Stock stock, BrokerRequest brokerRequest) {

        if (brokerRequest.getRequest().getRequestType().equals(RequestType.ASK)) {
            stock.getAskRequest().remove(brokerRequest);  //remove handled ask request

            BrokerRequest bidRequest = stock.FindHightestBidRequestInList();
            double priceOfTransaction = stock.FindHightestBidRequestInList().getRequest().getPricePerStock();
            stock.setLastTransictionPrice(priceOfTransaction);
            stock.getBidRequest().remove(bidRequest);//we also need to remove the bid request

            printMessage("Start a trade for stock " + stock.getCode() + " | " + stock.getName() + ".");
            printMessage("Bigest bid was " + stock.getHightestBid().getRequest().getPricePerStock() + "€");
            printMessage("Lowest ask was " + stock.getLowestAsk().getRequest().getPricePerStock() + "€");
            Trade ourTrade = new Trade(brokerRequest.getRequest().getStockCode(), brokerRequest.getRequest().getPricePerStock(), bidRequest.getRequest().getRequestingClientName(), brokerRequest.getRequest().getRequestingClientName());
            tradeProcessed.add(ourTrade);

            String messageForSeller = "We found a buyer for your share : " + stock.getCode() + " | " + stock.getName() + " at : " + priceOfTransaction + "€";
            bidRequest.getClient().sendData(messageForSeller);

            String messageForBuyer = "You just buy a share of " + stock.getCode() + " | " + stock.getName() + " at : " + priceOfTransaction + "€";
            brokerRequest.getClient().sendData(messageForBuyer);

        } else if (brokerRequest.getRequest().getRequestType().equals(RequestType.BID)) {
            stock.getBidRequest().remove(brokerRequest); //remove handled request
            BrokerRequest askRequest = stock.FindLowestAskRequestInList();
            double priceOfTransaction = stock.FindLowestAskRequestInList().getRequest().getPricePerStock();
            stock.setLastTransictionPrice(priceOfTransaction);
            stock.getAskRequest().remove(askRequest); //we also need to remove the ask request

            printMessage("Start a trade for stock " + stock.getCode() + " | " + stock.getName() + ".");
            printMessage("Bigest bid was " + stock.getHightestBid().getRequest().getPricePerStock() + "€");
            printMessage("Lowest ask was " + stock.getLowestAsk().getRequest().getPricePerStock() + "€");
            Trade ourTrade = new Trade(brokerRequest.getRequest().getStockCode(), priceOfTransaction, brokerRequest.getRequest().getRequestingClientName(), askRequest.getRequest().getRequestingClientName());
            tradeProcessed.add(ourTrade);

            String messageForSeller = "We found a buyer for your share : " + stock.getCode() + " | " + stock.getName() + " at : " + priceOfTransaction + "€";
            brokerRequest.getClient().sendData(messageForSeller);

            String messageForBuyer = "You just buy a share of " + stock.getCode() + " | " + stock.getName() + " at : " + priceOfTransaction + "€";
            askRequest.getClient().sendData(messageForBuyer);
        } else {
            throw new IllegalStateException("Start trade > Unkown request type");
        }

        //now we need to refill bigest bid and lowest ask for the stock
        stock.RefillLowestAskAndBigestBid();
    }

    @Override
    public void serverInfoMessage(String message) {
        printMessage("SERVER INFO : " + message);
    }

    @Override
    public void serverErrorMessage(String message) {
        printError("ERROR INFO : " + message);
    }

    @Override
    public void serverClientMessage(ClientService client, String message) {

        printMessage("SERVER RECEIVED : " + message);
        JSONParser jsonParser = new JSONParser();
        Request newRequest = jsonParser.fromStringToObject(message);

        BrokerRequest brokerRequest = new BrokerRequest(client, newRequest);

        this.HandleRequest(brokerRequest);
        brokerRequest.getClient().sendData("We received your request... Please wait");

    }

    public void printMessage(String message) {
        if (gui != null) {
            gui.getConsole().log(message);
        }
    }

    public void printError(String message) {
        if (gui != null) {
            gui.getConsole().logErr(message);
        }
    }

    /**
     * @return the stocks
     */
    public static ArrayList<Stock> getStocks() {
        return stocks;
    }

    /**
     * @param stocks the stocks to set
     */
    public void setStocks(ArrayList<Stock> stocks) {
        this.stocks = stocks;
    }

    /**
     * @return the tradeProcessed
     */
    public ArrayList<Trade> getTradeProcessed() {
        return tradeProcessed;
    }

    /**
     * @param tradeProcessed the tradeProcessed to set
     */
    public void setTradeProcessed(ArrayList<Trade> tradeProcessed) {
        this.tradeProcessed = tradeProcessed;
    }

}
