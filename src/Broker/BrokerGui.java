package Broker;

import Logger.Logger;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Tools.Console;
import Trade.Trade;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class BrokerGui extends JFrame {

    private JPanel contentPane;
    private Console console;
    public static Broker broker;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    BrokerGui frame = new BrokerGui();
                    frame.setVisible(true);
                    Broker broker = new Broker(frame);
                    frame.setBroker(broker);
                    broker.startXMLRPC();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public BrokerGui() {
        setTitle("BROKER");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        console = new Console();
        contentPane.add(console, BorderLayout.CENTER);

        JButton btnSaveLog = new JButton("Save history to log");
        btnSaveLog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                if (broker != null) {
                    ArrayList<Trade> tradesProcessed;
                    tradesProcessed = broker.getTradeProcessed();
                    Logger.writeToCSV(tradesProcessed);
                }
            }
        });
        contentPane.add(btnSaveLog, BorderLayout.NORTH);

    }

    public Console getConsole() {
        return console;
    }

    public void setConsole(Console console) {
        this.console = console;
    }

    /**
     * @return the broker
     */
    public Broker getBroker() {
        return broker;
    }

    /**
     * @param broker the broker to set
     */
    public void setBroker(Broker broker) {
        this.broker = broker;
    }

}
