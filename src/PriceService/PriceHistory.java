package PriceService;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.RainbowPalette;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.general.SeriesException;
import org.jfree.data.time.Millisecond;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.xy.XYDataset;

import Core.RPCXML.RPCXMLClient;

public class PriceHistory extends JFrame {

    private PriceServiceGui gui;
    private RPCXMLClient client;
    private String code;
    private Thread autoUpdateThread;

    private TimeSeries series;
    private String lastDataSet;

    /**
     * Create the frame.
     */
    public PriceHistory(PriceServiceGui gui, RPCXMLClient client, String[] rawData, String code) {
        setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        setBounds(100, 100, 450, 300);

        this.client = client;
        this.gui = gui;
        this.code = code;

        final XYDataset dataset = createDataset(rawData);
        final JFreeChart chart = createChart(dataset, code);
        final ChartPanel chartPanel = new ChartPanel(chart);
        chartPanel.setPreferredSize(new java.awt.Dimension(560, 370));
        chartPanel.setMouseZoomable(true, false);
        setContentPane(chartPanel);

        setTitle("Historical & Live values");

        autoUpdateThread = new Thread() {
            public void run() {
                while (true) {

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    String newData = client.getLastTransaction(code);

                    if (!lastDataSet.equals(newData)) {
                        lastDataSet = newData;
                        appendDataSet(newData);
                    }

                }
            }
        };

        autoUpdateThread.start();

        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                if (autoUpdateThread != null) {
                    autoUpdateThread.stop();
                }
                dispose();
            }
        });

    }

    private void appendDataSet(String rawData) {

        String[] data = rawData.split("\\|");

        if (data.length > 1) {
            Date date = new Date(Long.parseLong(data[0]));
            Millisecond current = new Millisecond(date);
            double value = new Double(Double.parseDouble(data[1]));
            series.add(current, value);
            gui.updatePriceSelected(code, value);
        }

    }

    private XYDataset createDataset(String[] rawData) {

        series = new TimeSeries("Stock price");

        for (int i = 0; i < rawData.length; i++) {

            System.out.println(rawData[i]);

            lastDataSet = rawData[i];

            String[] data = rawData[i].split("\\|");

            if (data.length > 1) {
                Date date = new Date(Long.parseLong(data[0]));
                Millisecond current = new Millisecond(date);
                series.add(current, new Double(Double.parseDouble(data[1])));
            }

        }

        return new TimeSeriesCollection(series);
    }

    private JFreeChart createChart(final XYDataset dataset, String code) {
        return ChartFactory.createTimeSeriesChart(
                code + " live stock price",
                "Time",
                "Price",
                dataset,
                false,
                false,
                false);
    }

}
