package PriceService;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import Core.RPCXML.RPCClientListener;
import Core.RPCXML.RPCXMLClient;
import Tools.Addresses;
import Tools.Console;

import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;

import java.awt.event.ActionListener;
import java.util.Random;
import java.util.UUID;
import java.awt.event.ActionEvent;
import javax.swing.JSplitPane;
import javax.swing.JScrollPane;
import javax.swing.JList;
import net.miginfocom.swing.MigLayout;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;
import java.awt.Label;

public class PriceServiceGui extends JFrame implements RPCClientListener {

    private Console console;
    private RPCXMLClient client;

    private JComboBox comboBox;
    private String selectedAdress = "localhost";
    private DefaultListModel stockListModel = new DefaultListModel();

    private JPanel contentPane;
    private JSplitPane splitPane;
    private JPanel panel;
    private JPanel panel_1;
    private JPanel panel_2;
    private JScrollPane scrollPane;
    private JButton btnRefreshAvailable;
    private JList list;
    private JLabel lblStockName;
    private JLabel lblStockFullname;
    private JLabel lblStockRate;
    private JLabel label;
    private JLabel label_1;
    private JLabel label_2;
    private JLabel label_3;
    private JLabel label_4;
    private JLabel labelFishe;
    private JSplitPane splitPane_1;
    private JSplitPane splitPane_2;
    private JPanel panel_3;
    private JButton btnConnect;
    private JButton btnGetHistoricalValues;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    PriceServiceGui frame = new PriceServiceGui();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public PriceServiceGui() {
        setTitle("PRICE SERVICE");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 1000, 800);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        PriceServiceGui thisObj = this;

        splitPane = new JSplitPane();
        splitPane.setResizeWeight(0.5);
        contentPane.add(splitPane, BorderLayout.CENTER);

        panel_2 = new JPanel();
        splitPane.setLeftComponent(panel_2);
        panel_2.setLayout(new BorderLayout(0, 0));

        panel = new JPanel();
        contentPane.add(panel, BorderLayout.NORTH);
        panel.setLayout(new BorderLayout(0, 0));

        comboBox = new JComboBox();
        panel.add(comboBox);
        comboBox.setModel(new DefaultComboBoxModel(new String[]{"localhost", "ANTO", "JOJO", "MOMO", "PHIL"}));

        btnConnect = new JButton("Connect");
        btnConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                client = new RPCXMLClient(thisObj, selectedAdress);
                refreshStocks();

            }
        });
        panel.add(btnConnect, BorderLayout.EAST);

        comboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                JComboBox<String> combo = (JComboBox<String>) event.getSource();
                String result = (String) combo.getSelectedItem();

                switch (result) {
                    case "localhost":
                        selectedAdress = "localhost";
                    case "ANTO":
                        selectedAdress = Addresses.IP_ANTO;
                    case "JOJO":
                        selectedAdress = Addresses.IP_JOJO;
                    case "MOMO":
                        selectedAdress = Addresses.IP_MOMO;
                    case "PHIL":
                        selectedAdress = Addresses.IP_PHIL;
                }
            }
        });

        ImageIcon icon = new ImageIcon("src/res/schleimfishe.JPG");

        splitPane_1 = new JSplitPane();
        splitPane_1.setResizeWeight(0.5);
        splitPane_1.setOrientation(JSplitPane.VERTICAL_SPLIT);
        panel_2.add(splitPane_1, BorderLayout.CENTER);

        scrollPane = new JScrollPane();
        splitPane_1.setLeftComponent(scrollPane);

        btnRefreshAvailable = new JButton("Refresh available");
        btnRefreshAvailable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                refreshStocks();

            }
        });
        scrollPane.setColumnHeaderView(btnRefreshAvailable);

        list = new JList(stockListModel);
        list.addListSelectionListener(new ListSelectionListener() {

            @Override
            public void valueChanged(ListSelectionEvent arg0) {
                if (!arg0.getValueIsAdjusting() && list.getSelectedValue() != null) {
                    if (client != null) {
                        lblStockName.setText(list.getSelectedValue().toString());
                        lblStockFullname.setText(client.getStockName(list.getSelectedValue().toString()));
                        lblStockRate.setText(client.singleStockPrice(list.getSelectedValue().toString()) + "€");
                    }
                }
            }

        });
        scrollPane.setViewportView(list);

        console = new Console();
        splitPane_1.setRightComponent(console);

        splitPane_2 = new JSplitPane();
        splitPane_2.setResizeWeight(0.5);
        splitPane.setRightComponent(splitPane_2);
        splitPane_2.setOrientation(JSplitPane.VERTICAL_SPLIT);

        panel_3 = new JPanel();
        splitPane_2.setRightComponent(panel_3);
        panel_3.setLayout(new BorderLayout(0, 0));

        labelFishe = new JLabel();
        panel_3.add(labelFishe, BorderLayout.CENTER);
        labelFishe.setHorizontalAlignment(SwingConstants.CENTER);
        labelFishe.setIcon(icon);

        panel_1 = new JPanel();
        splitPane_2.setLeftComponent(panel_1);
        panel_1.setLayout(new MigLayout("", "[left][grow,fill][right]", "[][][][][][][][][][][]"));

        btnGetHistoricalValues = new JButton("Get historical & live values");
        btnGetHistoricalValues.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {
                if (list.getSelectedValue() != null) {
                    if (client != null) {
                        String[] history = client.getStockHistory(list.getSelectedValue().toString());
                        PriceHistory frame = new PriceHistory(thisObj, client, history, list.getSelectedValue().toString());
                        frame.setVisible(true);
                    }
                }
            }
        });
        panel_1.add(btnGetHistoricalValues, "cell 1 0");

        label = new JLabel("   ");
        panel_1.add(label, "cell 0 1");

        lblStockName = new JLabel("");
        lblStockName.setFont(new Font("Raavi", Font.BOLD, 30));
        lblStockName.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblStockName, "cell 1 2,alignx center");

        lblStockFullname = new JLabel("");
        lblStockFullname.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblStockFullname, "cell 1 3,alignx center");

        lblStockRate = new JLabel("");
        lblStockRate.setFont(new Font("Tahoma", Font.PLAIN, 16));
        lblStockRate.setHorizontalAlignment(SwingConstants.CENTER);
        panel_1.add(lblStockRate, "cell 1 5,alignx center");

        label_2 = new JLabel("   ");
        panel_1.add(label_2, "cell 0 6");

        label_3 = new JLabel("   ");
        panel_1.add(label_3, "cell 0 7");

        label_4 = new JLabel("   ");
        panel_1.add(label_4, "cell 0 8");

        label_1 = new JLabel("   ");
        panel_1.add(label_1, "cell 2 10");

    }

    public void updatePriceSelected(String code, double value) {
        if (list.getSelectedValue() != null) {
            if (list.getSelectedValue().toString().equals(code)) {
                lblStockRate.setText(value + "€");
            }
        }
    }

    public void refreshStocks() {

        if (client != null) {

            String[] stocks = client.getStockNames();

            stockListModel.clear();

            for (int i = 0; i < stocks.length; i++) {

                stockListModel.addElement(stocks[i]);

            }

        }

    }

    public Console getConsole() {
        return console;
    }

    public void setConsole(Console console) {
        this.console = console;
    }

    @Override
    public void RPCClientMessage(String message) {
        if (console != null) {
            console.log(message);
        }
    }

    @Override
    public void RPCClientError(String message) {
        if (console != null) {
            console.logErr(message);
        }
    }

}
