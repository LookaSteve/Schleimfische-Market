/**
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package Tests.MOM;

import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;

import javax.jms.*;
import java.util.Random;
import java.util.concurrent.TimeUnit;

class Publisher {

    public static void main(String[] args) throws JMSException {

        String user = env("ACTIVEMQ_USER", "admin");
        String password = env("ACTIVEMQ_PASSWORD", "password");
        String host = env("ACTIVEMQ_HOST", "localhost");
        int port = Integer.parseInt(env("ACTIVEMQ_PORT", "61616"));
        String destination = arg(args, 0, "event");

        int messages = 20;

        String body = "A news has been published";

        ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);

        Connection connection = factory.createConnection(user, password);
        connection.start();
        Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
        Destination dest = new ActiveMQTopic(destination);
        MessageProducer producer = session.createProducer(dest);
        producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);

        Random randomGenerator = new Random();
        int random_news = 0;
        String[] code_stock = new String[4];
        code_stock[0] = "AAPL";
        code_stock[1] = "IBM";
        code_stock[2] = "MSFT";
        code_stock[3] = "ORCL";

        for (int i = 1; i <= messages; i++) {
            TextMessage msg = session.createTextMessage(body);
            msg.setIntProperty("id", i);

            // Generate good or bad news
            random_news = randomGenerator.nextInt(2);
            msg.setIntProperty("typeOfNews", random_news);

            random_news = randomGenerator.nextInt(4);
            msg.setStringProperty("codeStock", code_stock[random_news]);

            producer.send(msg);

            System.out.println("Send a news :");
            System.out.println(msg.toString());
            try {
                TimeUnit.MILLISECONDS.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println(e);
            }
        }

        producer.send(session.createTextMessage("SHUTDOWN"));
        connection.close();
    }

    private static String env(String key, String defaultValue) {
        String rc = System.getenv(key);
        if (rc == null) {
            return defaultValue;
        }
        return rc;
    }

    private static String arg(String[] args, int index, String defaultValue) {
        if (index < args.length) {
            return args[index];
        } else {
            return defaultValue;
        }
    }

}
