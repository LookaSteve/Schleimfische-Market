package Tests.NetDemo;

import java.awt.EventQueue;

import Core.Demo;
import Core.Client.Client;
import Core.Client.ClientListener;
import Core.Server.ClientService;
import Core.Server.Server;
import Core.Server.ServerListener;
import Tools.Addresses;

public class NetDemo implements ServerListener, ClientListener {

    public static void main(String[] args) {

        new NetDemo();

    }

    public NetDemo() {

        Server server = new Server(Addresses.PORT, this);
        server.start();

        sleep(1000);

        Client client = new Client("localhost", Addresses.PORT, this);
        sleep(1000);
        client.sendData("message de test !");

    }

    @Override
    public void clientInfoMessage(String message) {
        System.out.println("CLIENT INFO : " + message);
    }

    @Override
    public void clientErrorMessage(String message) {
        System.out.println("CLIENT ERROR : " + message);
    }

    @Override
    public void clientServerMessage(Client server, String message) {
        System.out.println("CLIENT RECEIVED : " + message);
    }

    @Override
    public void serverInfoMessage(String message) {
        System.out.println("SERVER INFO : " + message);
    }

    @Override
    public void serverErrorMessage(String message) {
        System.out.println("SERVER ERROR : " + message);
    }

    @Override
    public void serverClientMessage(ClientService client, String message) {
        System.out.println("SERVER RECEIVED : " + message);
        client.sendData(message);
    }

    public void sleep(int ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
