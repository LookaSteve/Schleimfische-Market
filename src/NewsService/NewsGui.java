package NewsService;

import Logger.Logger;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.jms.JMSException;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Tools.Console;
import Trade.Trade;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Random;

public class NewsGui extends JFrame {

    private JPanel contentPane;
    private Console console;
    private NewsGenerator newsServer;
    private Thread autoThread;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    NewsGui frame = new NewsGui();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public NewsGui() {
        setTitle("NEWS GENERATOR");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        console = new Console();
        contentPane.add(console, BorderLayout.CENTER);

        newsServer = new NewsGenerator(this);

        JButton btnSaveLog = new JButton("Toggle news generator");
        btnSaveLog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                if (newsServer != null) {
                    if (autoThread != null) {
                        autoThread.stop();
                        autoThread = null;
                        console.log("Automatic requests stopped.");
                    } else {
                        autoThread = new Thread() {
                            public void run() {
                                while (true) {
                                    Random rand = new Random();
                                    try {
                                        Thread.sleep((rand.nextInt(10) + 1) * 500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    newsServer.generateNews();
                                }
                            }
                        };

                        autoThread.start();
                        console.log("Automatic requests started.");
                    }
                } else {
                    console.logErr("The trader first needs to be started.");
                }
            }
        });
        contentPane.add(btnSaveLog, BorderLayout.NORTH);

    }

    public Console getConsole() {
        return console;
    }

    public void setConsole(Console console) {
        this.console = console;
    }

}
