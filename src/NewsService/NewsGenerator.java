package NewsService;

import javax.jms.JMSException;
import Core.MOM.MOMServer;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.print.attribute.standard.PrinterMessageFromOperator;

/**
 * Created by Momo on 01/03/2018.
 */
public class NewsGenerator {

    private NewsGui gui;
    private MOMServer server;

    public NewsGenerator(NewsGui newsGui) {
        this.gui = newsGui;
        try {
            this.server = new MOMServer();
        } catch (JMSException ex) {
            Logger.getLogger(NewsGenerator.class.getName()).log(Level.SEVERE, null, ex);
            printError(ex.getMessage());
        }
    }

    /**
     * Randomly generate a news and send it via a MOM Message
     */
    public void generateNews() {
        try {
            Random randomGenerator = new Random();
            int random;
            String[] codeStock = new String[4];
            codeStock[0] = "AAPL";
            codeStock[1] = "IBM";
            codeStock[2] = "MSFT";
            codeStock[3] = "ORCL";

            String chosenCode;
            String body;
            random = randomGenerator.nextInt(4);
            chosenCode = codeStock[random];
            random = randomGenerator.nextInt(2);

            if (random == 0) {
                body = "Bad news about  " + chosenCode;
            } else {
                body = "Good news about  " + chosenCode;
            }
            printMessage(body);
            System.out.println(body);

            server.sendNews(chosenCode, body, random);
            try {
                TimeUnit.MILLISECONDS.sleep(2000);
            } catch (InterruptedException e) {
                System.out.println(e);
                printError(e.getMessage());
            }

        } catch (JMSException ex) {
            Logger.getLogger(NewsGenerator.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void printMessage(String message) {
        if (gui != null) {
            gui.getConsole().log(message);
        }
    }

    public void printError(String message) {
        if (gui != null) {
            gui.getConsole().logErr(message);
        }
    }
}
