/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logger;

import Trade.Request;
import Trade.Trade;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * The broker should create a log file, showing all client requests processed.
 * Use a csv (comma separated values) format so that we can analyze the results
 * afterwards with a spreadsheet program.
 *
 * @author Karakayn
 */
public class Logger {

    private static final String CSV_SEPARATOR = ",";

    public static void writeToCSV(ArrayList<Trade> tradeList) {
        try {
            BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream("Log_trade.csv"), "UTF-8"));
            StringBuffer headerLine = new StringBuffer();
            headerLine.append("Transaction date");
            headerLine.append(CSV_SEPARATOR);
            headerLine.append("Stock code");
            headerLine.append(CSV_SEPARATOR);
            headerLine.append("Buyer name");
            headerLine.append(CSV_SEPARATOR);
            headerLine.append("Seller name");
            headerLine.append(CSV_SEPARATOR);
            headerLine.append("Transaction price");
            headerLine.append(CSV_SEPARATOR);
            bw.write(headerLine.toString());
            bw.newLine();
            for (Trade trade : tradeList) {
                StringBuffer oneLine = new StringBuffer();

                oneLine.append(trade.getDateOfTransaction());
                oneLine.append(CSV_SEPARATOR);
                oneLine.append(trade.getStockCode());
                oneLine.append(CSV_SEPARATOR);
                oneLine.append(trade.getNameOfBuyer());
                oneLine.append(CSV_SEPARATOR);
                oneLine.append(trade.getNameOfSeller());
                oneLine.append(CSV_SEPARATOR);
                oneLine.append(trade.getTransactionPrice());
                bw.write(oneLine.toString());
                bw.newLine();
            }
            bw.flush();
            bw.close();
        } catch (UnsupportedEncodingException e) {
            System.out.println(e.getMessage());
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
