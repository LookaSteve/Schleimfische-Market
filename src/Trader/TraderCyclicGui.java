package Trader;

import NewsService.NewsGenerator;
import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import Tools.Addresses;
import Tools.Console;
import Tools.NameGenerator;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.Random;
import java.util.UUID;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;

public class TraderCyclicGui extends JFrame {

    private JPanel contentPane;
    private Console console;
    private AdvancedTraderCyclic trader;
    private JComboBox comboBox;
    private JPanel panel;
    private JButton btnConnect;

    private String selectedAdress = "localhost";
    private JButton btnToggleAutomaticRequests;
    private Thread autoThread;

    /**
     * Launch the application.
     */
    public static void main(String[] args) {
        EventQueue.invokeLater(new Runnable() {
            public void run() {
                try {
                    TraderCyclicGui frame = new TraderCyclicGui();
                    frame.setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /**
     * Create the frame.
     */
    public TraderCyclicGui() {
        setTitle("TRADER CYCLIC");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setBounds(100, 100, 600, 400);
        contentPane = new JPanel();
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.setLayout(new BorderLayout(0, 0));
        setContentPane(contentPane);

        TraderCyclicGui thisObj = this;

        console = new Console();
        contentPane.add(console, BorderLayout.CENTER);

        panel = new JPanel();
        contentPane.add(panel, BorderLayout.NORTH);
        panel.setLayout(new BorderLayout(0, 0));

        comboBox = new JComboBox();
        panel.add(comboBox);
        comboBox.setModel(new DefaultComboBoxModel(new String[]{"localhost", "ANTO", "JOJO", "MOMO", "PHIL"}));

        comboBox.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent event) {
                JComboBox<String> combo = (JComboBox<String>) event.getSource();
                String result = (String) combo.getSelectedItem();

                switch (result) {
                    case "localhost":
                        selectedAdress = "localhost";
                    case "ANTO":
                        selectedAdress = Addresses.IP_ANTO;
                    case "JOJO":
                        selectedAdress = Addresses.IP_JOJO;
                    case "MOMO":
                        selectedAdress = Addresses.IP_MOMO;
                    case "PHIL":
                        selectedAdress = Addresses.IP_PHIL;
                }
            }
        });

        btnConnect = new JButton("Connect");
        btnConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                NameGenerator ng = new NameGenerator();
                trader = new AdvancedTraderCyclic(console, selectedAdress, ng.generateName());

            }
        });
        panel.add(btnConnect, BorderLayout.EAST);

        btnToggleAutomaticRequests = new JButton("Toggle automatic trading");
        btnToggleAutomaticRequests.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent arg0) {

                if (trader != null) {
                    if (autoThread != null) {
                        autoThread.stop();
                        autoThread = null;
                        console.log("Automatic requests stopped.");
                    } else {
                        autoThread = new Thread() {
                            public void run() {
                                while (true) {
                                    Random rand = new Random();
                                    try {
                                        Thread.sleep((rand.nextInt(10) + 1) * 500);
                                    } catch (InterruptedException e) {
                                        e.printStackTrace();
                                    }
                                    trader.ListenForNews();
                                }
                            }
                        };

                        autoThread.start();
                        console.log("Automatic requests started.");
                    }
                } else {
                    console.logErr("The trader first needs to be started.");
                }
            }

        });
        panel.add(btnToggleAutomaticRequests, BorderLayout.SOUTH);

    }

    public Console getConsole() {
        return console;
    }

    public void setConsole(Console console) {
        this.console = console;
    }

}
