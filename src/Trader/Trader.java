package Trader;

import Core.Client.Client;
import Core.Client.ClientListener;
import Tools.Addresses;
import Tools.Console;
import Trade.Request;
import java.util.Random;
import Tools.JSONParser;

/**
 *
 * @author Karakayn
 */
public class Trader implements ClientListener {

    private Console console;

    private Client client;
    private String name;

    public Trader(Console console, String address, String name) {
        this.console = console;
        this.name = name;
        this.client = new Client(address, Addresses.PORT, this);
    }

    public void sendRequest(Request r) {
        // Parse Object to send
        JSONParser parser = new JSONParser();
        String data = parser.fromObjectToString(r);

        client.sendData(data);
    }

    @Override
    public void clientInfoMessage(String message) {
        printMessage("CLIENT INFO : " + message);
    }

    @Override
    public void clientErrorMessage(String message) {
        printError("CLIENT ERROR : " + message);
    }

    @Override
    public void clientServerMessage(Client server, String message) {
        printMessage("CLIENT RECEIVED : " + message);
    }

    public void printMessage(String message) {
        if (console != null) {
            console.log(message);
        }
    }

    public void printError(String message) {
        if (console != null) {
            console.logErr(message);
        }
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

}
