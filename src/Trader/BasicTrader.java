package Trader;

import Trade.Request;
import java.util.Random;

import Tools.Console;

public class BasicTrader extends Trader {

    public BasicTrader(Console console, String address) {
        super(console, address, "Hans");
    }

    public BasicTrader(Console console, String address, String n) {
        super(console, address, n);
    }

    public void sendRequest() {
        // Generate random data
        Random randomGenerator = new Random();
        int random_action = randomGenerator.nextInt(2);
        int price = randomGenerator.nextInt(20);
        int code = randomGenerator.nextInt(5);

        String stockCode = "";
        String action = "";

        switch (code) {
            case 0:
                stockCode = "AAPL";
                break;
            case 1:
                stockCode = "IBM";
                break;
            case 2:
                stockCode = "MSFT";
                break;
            case 3:
                stockCode = "ORCL";
                break;
            case 4:
                stockCode = "TSLA";
                break;
        }

        if (random_action == 0) {
            action = "ASK";
        } else {
            action = "BID";
        }

        Request r = new Request(this.getName(), action, 1, price, stockCode);

        super.sendRequest(r);
    }
}
