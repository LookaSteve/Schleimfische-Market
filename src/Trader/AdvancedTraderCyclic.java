/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Trader;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;

import Tools.Console;

/**
 * Both new kinds of traders read the news, and decide to buy or sell based on
 * what they read. This kind of trader is cyclic: They sell when they read bad
 * news, and buy when they read good news.
 *
 * @author Karakayn
 */
public class AdvancedTraderCyclic extends AdvancedTrader {

    public AdvancedTraderCyclic(Console console, String address, String name) {
        super(console, address, name);
    }

    public void ListenForNews() {
        printMessage("Waiting for messages...");

        try {
            Message msg = consumer.receive();
            System.out.println("Msg > " + msg);
            int typeOfNews = 0;
            String codeStock = "Unknown";
            typeOfNews = msg.getIntProperty("typeOfNews");
            codeStock = msg.getStringProperty("codeStock");

            if (typeOfNews == 0) //it's a bad news
            {
                printMessage("Bad news about " + codeStock);
                super.sendRequest(codeStock, "ASK");
            } else if (typeOfNews == 1) //good news
            {
                printMessage("Good news about " + codeStock);
                super.sendRequest(codeStock, "BID");
            } else { //impossible
                throw new IllegalStateException("Impossible type of news");
            }

        } catch (JMSException ex) {
            Logger.getLogger(AdvancedTraderACyclic.class.getName()).log(Level.SEVERE, null, ex);

        }

    }
}
