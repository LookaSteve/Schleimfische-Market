package Trader;

import Tools.Addresses;
import Tools.Console;
import Trade.Request;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.Session;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQTopic;

public class AdvancedTrader extends Trader {

    protected Connection connection;
    protected MessageConsumer consumer;

    public AdvancedTrader(Console console, String address) {
        super(console, address, "Eva");
        connection = null;
        consumer = null;
        initializeMOMConnection(address);
    }

    public AdvancedTrader(Console console, String address, String name) {
        super(console, address, name);
        connection = null;
        consumer = null;
        initializeMOMConnection(address);
    }

    public void initializeMOMConnection(String address) {
        try {
            String user = env("ACTIVEMQ_USER", "admin");
            String password = env("ACTIVEMQ_PASSWORD", "password");
            String host = env("ACTIVEMQ_HOST", address);
            int port = Integer.parseInt(env("ACTIVEMQ_PORT", Integer.toString(Addresses.PORT_MOM)));
            String destination = "event";

            printMessage("Initialise connection with : " + user + ":" + password + "@" + host + ":" + port);
            ActiveMQConnectionFactory factory = new ActiveMQConnectionFactory("tcp://" + host + ":" + port);

            this.connection = factory.createConnection(user, password);
            connection.start();
            Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            Destination dest = new ActiveMQTopic(destination);

            this.consumer = session.createConsumer(dest);
        } catch (JMSException ex) {
            Logger.getLogger(AdvancedTrader.class.getName()).log(Level.SEVERE, null, ex);
            printError(ex.getMessage());
        }
    }

    private static String env(String key, String defaultValue) {
        String rc = System.getenv(key);
        if (rc == null) {
            return defaultValue;
        }
        return rc;
    }

    private static String arg(String[] args, int index, String defaultValue) {
        if (index < args.length) {
            return args[index];
        } else {
            return defaultValue;
        }
    }

    /**
     * Need the action to send the request because in function inherited
     * AdvancedTrader will choose it in function of news
     *
     * @param action
     */
    public void sendRequest(String stockCode, String action) {
        // Generate random data
        Random randomGenerator = new Random();
        int price = randomGenerator.nextInt(20);

        Request r = new Request(this.getName(), action, 1, price, stockCode);
        printMessage("Send request for " + stockCode + " at " + price + "€");
        super.sendRequest(r);
    }
}
